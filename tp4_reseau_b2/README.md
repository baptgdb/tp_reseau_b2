# **TP 4**

## Table of Contents

[TOC]

## **I. First steps**

la commande "netstat -nb" liste les Connexions actives :

* *Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP :*

1. La mise a jour des pilotes nvidia utilise le ptotocole TCP
```
[nvcontainer.exe]
  TCP    192.168.1.16:14391     34.149.211.227:443     ESTABLISHED
```
[Nvidia_pilotes_update](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/Nvidia_pilotes_update.pcapng)

2. Discord utilise le ptotocole TCP
```
 [Discord.exe]
  TCP    192.168.1.16:33260     34.149.211.227:443     ESTABLISHED
```
[Discord](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/Discord.pcapng)

3. Fusion 360 qui fonctionne en drive utilise le ptotocole TCP
```
 [Fusion360.exe]
  TCP    192.168.1.16:34166     34.120.208.123:443     ESTABLISHED
```
[Fusion360](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/Fusion360.pcapng)

4. Firefox utilise le ptotocole TCP
```
 [firefox.exe]
  TCP    192.168.1.16:34183     3.67.249.49:443        ESTABLISHED
```
[Firefox](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/Firefox.pcapng)

5. Ainsi que le Launcher d'Ankama Game utilise le ptotocole TCP
```
 [Ankama Launcher.exe]
  TCP    192.168.1.16:34536     68.232.34.52:443       TIME_WAIT
```
[Ankama Launcher](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/Ankama_Launcher.pcapng)

6. petit bonus OneDrive utilise le ptotocole TCP
```
 [OneDrive.exe]
  TCP    192.168.1.16:28584     20.199.120.151:443     ESTABLISHED
```
[OneDrive](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/OneDrive.pcapng)

au final, je n'ai pas trouvé de requètes UDP 

## **II. Mise en place**

### **1. SSH**
on peut voir qu'une connection ssh utilise trois protocols:
* le protocole arp
* le protocole SSHv2 
* et le protocole TCP

on peut en déduire que les protocols SSHv2 et TCP permettent de sécuriser les comunications par SSH.

[ssh](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/ssh.pcapng)

[SSH TCP](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/SSH_TCP.pcapng)

on peut y voir les requètes [SYN], [SYN, ACK], [ACK] et [RST? ACK].

### **2. NFS**


1. 10.3.1.3(server)
* sudo dnf -y install nfs-utils 
* sudo nano /etc/idmapd.conf
* sudo nano /etc/exports
```
/home/nfsshare 10.3.1.0/24(rw,no_root_squash)
```
* sudo mkdir /home/nfsshare 
* systemctl enable --now rpcbind nfs-server 
* sudo firewall-cmd --add-service=nfs
* sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind} 
* sudo firewall-cmd --runtime-to-permanent 
* sudo mkdir /srv/fichier


#### la partie client ne fonctionne pas, je n'ai pas compris ce qu'il fallait faire avec les noms de domaines et j'ai pas trouvé avec quoi remplacer le nom de domaine.
**j'ai quand même essayé de faire la partie NFS avec les commandes mais ça ne fonctionne pas.** 
#### **en revanche la partie dns est faite**

2. 10.3.1.4(client)
* sudo dnf -y install nfs-utils 
* sudo mkdir /srv/fichier
sudo mount -t nfs tp4_client:/home/ftp /srv/fichier
```
mount.nfs: Failed to resolve server tp4_client: Name or service not known
```
* df -hT
```
Filesystem          Type      Size  Used Avail Use% Mounted on
devtmpfs            devtmpfs  462M     0  462M   0% /dev
tmpfs               tmpfs     481M     0  481M   0% /dev/shm
tmpfs               tmpfs     193M  3.0M  190M   2% /run
/dev/mapper/rl-root xfs       6.2G  1.2G  5.1G  18% /
/dev/sda1           xfs      1014M  210M  805M  21% /boot
tmpfs               tmpfs      97M     0   97M   0% /run/user/1000
```
* sudo mount -t nfs -o vers=3 tp4_client:/home/nfsshare /srv/fichier 
```
mount.nfs: Failed to resolve server tp4_client: Name or service not known
```
* df -hT /srv/fichier
```
Filesystem          Type  Size  Used Avail Use% Mounted on
/dev/mapper/rl-root xfs   6.2G  1.2G  5.1G  18% /
```
* sudo nano /etc/fstab 
```
...
tp4_client:/home/nfsshare /srv/fichier               nfs     defaults        0 0
```
* sudo dnf -y install autofs 
* sudo nano /etc/auto.master 
```
/-    /etc/auto.mount
```
* sudo nano /etc/auto.mount 
```
/srv/fichier   -fstype=nfs,rw  tp4_client:/home/nfsshare
```
* sudo systemctl enable --now autofs
```
Created symlink /etc/systemd/system/multi-user.target.wants/autofs.service → /usr/lib/systemd/system/autofs.service.
```
* grep /srv/fichier /proc/mounts
```
/etc/auto.mount /srv/fichier autofs rw,relatime,fd=17,pgrp=1127,timeout=300,minproto=5,maxproto=5,direct,pipe_ino=21871 0 0
```
bon ça ne fonctionne pas.


### **3. DNS**

[DNS](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp4_reseau_b2/DNS.pcapng)

* En effectuant directement une requête DNS avec dig :
* dig gitlab.com


## fin
