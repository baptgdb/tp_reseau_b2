# TP 5 Sécu défensive : VPN

![](https://i.redd.it/xa41rkrjk5x11.jpg)

## Table of Contents

[TOC]

### projet en groupe:

* **baptiste gadebille**
* **thomas eveillard**


### Les vm:

TP_5_vpn_server = 10.3.1.6/24

TP_5_vpn_client = 10.3.2.6/24

TP_5_router = 10.3.1.254/24 et 10.3.2.254/24

I. WireGuard serveur.

## I. WireGuard serveur.

### 1. Les firewall.

* On commence par les firewall:

sudo  dnf install firewalld -y
sudo systemctl start firewalld

sudo systemctl status firewalld
```
● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; ve>
     Active: active (running) since Mon 2022-10-17 16:43:59 CEST; 27min ago
...
```
sudo firewall-cmd --permanent --list-all

sudo firewall-cmd --reload
```
success
```
- Les firewall fonctionnent.

### 2. Paramétrage de WireGuard server.

1. On install le paquets pour "WireGuard".
```
sudo dnf install elrepo-release epel-release
```
2. On instale "WireGuard".
```
sudo dnf install kmod-wireguard wireguard-tools
```
3. WireGuard est installé.


### 2.1. Paramétrage de WireGuard server (clé).


1. Nous créons la clé privée pour WireGuard.
wg genkey | sudo tee /etc/wireguard/private.key
```
iHsDkxOTZ7SmtqdO5Zr9aSDOCBQyLJDbh4QY9WaYXEo=
```
2. Nous changeons les autorisations de la clé privée.
sudo chmod go= /etc/wireguard/private.key
* Ce qui supprime toutes les autorisations sur le fichier pour les utilisateurs et les groupes autres que root afin de s'assurer que lui seul puisse accéder à la clé privée.

3. Maintenant la clé public.
sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key
```
99YkyDO084ggJP9Bq+faFVlfGql/2jK6/4BPMMoaqiY=
```

### 2.2. Paramétrage de WireGuard server (ip range).

1. Configuration de l'ipv4.

- ipv4 = 10.3.15.1/24

* Il est aussi possible de configurer l'adresse ipv6 mais ce n'est pas nécessaire dans notre cas.

### 3. Configuration de l'interface du serveur WireGuard.

* A cette étape, nous allons utilise les adresses ipv4


sudo nano /etc/wireguard/wg0.conf
```
[Interface]
PrivateKey = iHsDkxOTZ7SmtqdO5Zr9aSDOCBQyLJDbh4QY9WaYXEo=
Address = 10.3.15.1/24
ListenPort = 51820
SaveConfig = true
```

* Dans notre cas nous n'utiliserons pas l'ipv6, mais il est possible de l'ajouter dans le fichier de configuration, a la suite de l'adresse IPv4.

### 4.Cconfiguration réseau du serveur WireGuard.

* Nous allons activer l'IP forwarding qui permet de faire suivre des paquets comme le fait un routeur a travers plusieurs réseaux dans notre exemple 10.3.1.0 et le réseau privé virtuel.

sudo nano /etc/sysctl.conf
```
...
net.ipv4.ip_forward=1
```
* dans le cas ou on veut utiliser les adresse ipv6 il suffit d'ajouter "net.ipv6.conf.all.forwarding=1" en bas du fichier. "/etc/sysctl.conf" comme "net.ipv4.ip_forward=1" pour l'ipv4 mais dans notre cas nous n'allons pas utiliser l'ipv6.


sudo sysctl -p
```
net.ipv4.ip_forward = 1
```

### 5. Configuration du pare-feu du serveur WireGuard.

1. pour autoriser l'accès permanent au service WireGuard sur le port UDP 51820 :
```
sudo firewall-cmd --zone=public --add-port=51820/udp --permanent
```

2. ajouter l'interface wg0 à la zone interne
```
 sudo firewall-cmd --zone=internal --add-interface=wg0 --permanent
success
```

3. On active le masquage de nos plages réseau IPv4:
```
sudo firewall-cmd --zone=public --add-rich-rule='rule family=ipv4 source address=10.3.15.1/24 masquerade' --permanent
```
sudo firewall-cmd --reload

* On peut examiner la place public :

sudo firewall-cmd --zone=public --list-all
```
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 51820/udp 				///vpn port
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
        rule family="ipv4" source address="10.3.1.1/24" masquerade
```

* On peut vérifier que la règle de transfert interne est en place :

sudo firewall-cmd --zone=internal --list-interfaces
```
wg0
```

### 6. Démarrage du serveur WireGuard.

1. Nous allons configurer WireGuard pour qu'il démarre au démarrage pour pouvoir nous connecter au VPN à tout moment tant que le serveur est disponible. 
- Nous allons activez le service wg-quick pour que le tunnel wg0 soit ajouté à systemctl :

```
sudo systemctl enable wg-quick@wg0.service

Created symlink /etc/systemd/system/multi-user.target.wants/wg-quick@wg0.service → /usr/lib/systemd/system/wg-quick@.service.
```

1.1. on vas lancer le service "wg-quick" : 
```
sudo systemctl start wg-quick@wg0.service
```

on vérifie l'étét du service : 
```
sudo systemctl status wg-quick@wg0.service

   Active: active (exited) since Tue 2022-10-18 14:50:17 CEST; 5min ago
```

## II. on ajoute un router

### 1. router et ip forward

sudo nano /etc/sysctl.d/routeur.conf
```
net.ipv4.ip_forward=1
```
* comme pour le serveur vpn, on active l'ip forward.

sudo sysctl -p /etc/sysctl.d/routeur.conf
```
net.ipv4.ip_forward = 1
```

cat /proc/sys/net/ipv4/ip_forward
```
1
```
* l'ip forward est activé.

### 2. testons si on peut se ping

* on peut ping la TP_5_vpn_server depuis TP_5_vpn_client grace au router (TP_5_route)
ping 10.3.1.6
```
PING 10.3.1.6 (10.3.1.6) 56(84) bytes of data.
64 bytes from 10.3.1.6: icmp_seq=1 ttl=63 time=0.997 ms
64 bytes from 10.3.1.6: icmp_seq=2 ttl=63 time=2.40 ms
64 bytes from 10.3.1.6: icmp_seq=3 ttl=63 time=2.29 ms
64 bytes from 10.3.1.6: icmp_seq=4 ttl=63 time=2.38 ms
--- 10.3.1.6 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 0.997/2.016/2.399/0.592 ms
```
* on peut ping la TP_5_vpn_client depuis TP_5_vpn_server grace au router (TP_5_route)

ping 10.3.2.6
```
PING 10.3.2.6 (10.3.2.6) 56(84) bytes of data.
64 bytes from 10.3.2.6: icmp_seq=1 ttl=63 time=1.16 ms
64 bytes from 10.3.2.6: icmp_seq=2 ttl=63 time=1.41 ms
64 bytes from 10.3.2.6: icmp_seq=3 ttl=63 time=2.14 ms
64 bytes from 10.3.2.6: icmp_seq=4 ttl=63 time=1.36 ms
--- 10.3.2.6 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 1.161/1.519/2.141/0.371 ms
```

## III. WireGuard client.

### 1. Paramétrage de WireGuard client.

comme pour le paramétrage de WireGuard server:
```
sudo dnf install elrepo-release epel-release
...
sudo dnf install kmod-wireguard wireguard-tools
...
```

### 2. Paramétrage de WireGuard server (clé).

toujours comme pour le paramétrage de WireGuard server (clé).

* La clé privée :
wg genkey | sudo tee /etc/wireguard/private.key
```
4HSzY4GV00jGfDpk/RAqpAfJkH0Bm4swSv3FGo9xkUg=
```
* sudo chmod go= /etc/wireguard/private.key

* La clé publique :
sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key
```
732JJxyYrdlekBU5HJ9hJ5HgD4gWp/bMXWsrjwKWnh0=
```

### 3. Paramétrage de WireGuard client (ip range).

```
/etc/wireguard/wg0.conf
[Interface]
PrivateKey = base64_encoded_peer_private_key_goes_here
Address = adresse du client sur dans le vpn

[Peer]
PublicKey = base64_encoded_server_public_key_goes_here
AllowedIPs = réseau_du_vpn/24, fd0d:86fa:c3bc::/64
Endpoint = adresse_de_la_vm_server:51820
```

* sudo nano /etc/wireguard/wg0.conf
```
[Interface]
PrivateKey = 4HSzY4GV00jGfDpk/RAqpAfJkH0Bm4swSv3FGo9xkUg=
Address = 10.3.15.2/24

[Peer]
PublicKey = 99YkyDO084ggJP9Bq+faFVlfGql/2jK6/4BPMMoaqiY=
AllowedIPs = 10.3.15.0
Endpoint = 10.3.1.6:51820
```

### 4. Configuration du client pour acheminer tout le trafic sur le tunnel

on affiche la table de la liste des routes :
ip route list table main default
```
default via 10.0.2.2 dev enp0s3 proto dhcp src 10.0.2.15 metric 100
default via 10.3.2.254 dev enp0s8 proto static metric 101
```


ip -brief address show enp0s8
```
enp0s8           UP             10.3.2.6/24 fe80::a00:27ff:fe32:1a3/64
```

### 5. Configuration du DNS sur le client WireGuard.

TP_5_vpn_server a comme serveur DNS :

cat /etc/resolv.conf
```
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
```

sudo nano /etc/wireguard/wg0.conf
```
DNS = 1.1.1.1 8.8.4.4 8.8.8.8

[Peer]
. . .
```
sudo systemctl enable systemd-resolved
sudo reboot

## IV Ajout de la clé publique du client au serveur.

### 1. clé public client

sudo cat /etc/wireguard/public.key
```
732JJxyYrdlekBU5HJ9hJ5HgD4gWp/bMXWsrjwKWnh0=
```

### 2. depuis le server

1. on ajoute la clé public du client sur le serveur.
```
sudo wg set wg0 peer 732JJxyYrdlekBU5HJ9hJ5HgD4gWp/bMXWsrjwKWnh0= allowed-ips 10.3.15.2
```

2. on autorise l'adresses IP "10.3.15.2/24"
sudo wg set wg0 peer 732JJxyYrdlekBU5HJ9hJ5HgD4gWp/bMXWsrjwKWnh0= allowed-ips 10.3.15.2

3. vérifions l'état du tunnel sur le serveur :
```
sudo wg

...
interface: wg0
  public key: 99YkyDO084ggJP9Bq+faFVlfGql/2jK6/4BPMMoaqiY=
  private key: (hidden)
  listening port: 51820

peer: 732JJxyYrdlekBU5HJ9hJ5HgD4gWp/bMXWsrjwKWnh0=
  allowed ips: 10.3.15.2/32
```

## V. Connexion VPN du client au tunnel vpn.

### 1. connexion au vpn depuis le client avec "sudo wg-quick up wg0"
```
sudo wg-quick up wg0
```
ip a

```
...
11: wg0: <POINTOPOINT,NOARP,UP,LOWER_UP> mtu 1420 qdisc noqueue state UNKNOWN group default qlen 1000
    link/none
    inet 10.3.15.1/24 scope global wg0
       valid_lft forever preferred_lft forever
```
- et voila ça fonctionne.


## 2. voyons si on peut ping le serveur dns 8.8.8.8 depuis le réseau du vpn.
ping 8.8.8.8
```
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=27.1 ms
...

64 bytes from 8.8.8.8: icmp_seq=10 ttl=113 time=19.8 ms

--- 8.8.8.8 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9019ms
rtt min/avg/max/mdev = 18.721/21.509/27.087/2.599 ms
```

bon contrairement a ce qui etait prévu ça fonctionne même si on changed'ip

## VI. amusons nous avec traceroute, avec les tables arp... et wireshark depuis le client.

### 1. traceroute:

- 1. lorsque l'interface enp0s3 du client et du router, est désactivé, on peut observer la route entre entre 10.3.2.6 et 10.3.1.6 :

```
traceroute 10.3.1.6
traceroute to 10.3.1.6 (10.3.1.6), 30 hops max, 60 byte packets
 1  * * *
 2  * * *
 3  * * *
 4  * * *
 5  * * *
 6  * * *
 7  * * *
 8  * * *
 9  * * *
10  * * *
11  * * client (10.3.2.6)  3085.486 ms !H
```

- 2. lorsque l'interface enp0s3 du client et du router, est désactivé, on peut observer la route entre entre 10.3.15.2 et 10.3.15.1 :

```
traceroute 10.3.15.1
traceroute to 10.3.15.1 (10.3.15.1), 30 hops max, 60 byte packets
 1  client (10.3.2.6)  3051.361 ms !H  3051.329 ms !H  3051.325 ms !H
```

### 2. les tables ARP

```
sudo arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.3.2.1                 ether   0a:00:27:00:00:0b   C                     enp0s8
_gateway                 ether   52:54:00:12:35:02   C                     enp0s3
```
le protocole arp lie les adresses Mac et les adresses Ip, mais notre VPN utilise un protocole de niveau 2, qui ne prend que les adresses Ip.  

### 3. Wireshark !

- observons les trames qui sont échangées pendant l'établissement du tunnel vers le vpn.
```
sudo tcpdump -n -w tp5cli.pcap | sudo wg-quick up wg0
```

[tunnel_vpn](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp5_reseau_b2/tunnel_vpn.pcap)

lors de l'établissement du tunnel, nous constatons l'usage de deux protocoles :
* NTP (« protocole de temps réseau ») entre le serveur VPN et 4 autres IP :
    * 135.125.169.44
        ```
        nslookup 135.125.169.44
        
        Nom :    ntp24.kashra-server.com
        Address:  135.125.169.44
        ```
    * 91.121.68.116
        ```
        nslookup 91.121.68.116
        
        Nom :    siberut.ordimatic.net
        Address:  91.121.68.116
        ```
    * 163.172.150.183
        ```
        nslookup 163.172.150.183
        
        Nom :    183-150-172-163.instances.scw.cloud
        Address:  163.172.150.183
        ```
    * 37.59.63.125
        ```
        nslookup 37.59.63.125
        
         Non-existent domain
        ```
    * NTP est un protocole qui permet de synchroniser, via un réseau informatique, l'horloge locale d'une machine.

* ARP (protocole de résolution d'adresse)
    * les requètes ARP concernent 10.0.2.2 et 10.0.2.15.
    * ARP est un protocole qui permet de faire correspondre une adresse ip  à une adresse mac.

### fin :

![](https://cdn.discordapp.com/attachments/928569185028669481/1033039703357804674/vpncat.jpg)

## **FIN**
