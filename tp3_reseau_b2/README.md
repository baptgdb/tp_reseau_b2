# **TP3**

## Table of Contents

[TOC]

## **I. ARP**

marcel:

* un ping entre marcel et john fonctionne 
```
[baptiste@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.441 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=1.16 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=1.08 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=0.710 ms
64 bytes from 10.3.1.11: icmp_seq=5 ttl=64 time=1.10 ms

--- 10.3.1.11 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4012ms
rtt min/avg/max/mdev = 0.441/0.898/1.159/0.278 ms
```

* voici la table arp de marcel:
```
[baptiste@localhost ~]$ sudo arp
[sudo] password for baptiste:
Address                  HWtype  HWaddress           Flags Mask            Iface
_gateway                         (incomplete)                              enp0s8
10.3.1.1                 ether   0a:00:27:00:00:0b   C                     enp0s8
10.3.1.11                ether   08:00:27:45:cf:67   C                     enp0s8
```

* voici l'adresse MAC de john:

```
ip a
    ...
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:45:cf:67 brd ff:ff:ff:ff:ff:ff
    ...
```
* l'adresse mac john est "08:00:27:45:cf:67" on la retrouve bien dans la table arp de marcel.


john:

* un ping entre john et marcel fonctionne 

```
[baptiste@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.407 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.769 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.475 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.894 ms
64 bytes from 10.3.1.12: icmp_seq=5 ttl=64 time=0.660 ms

--- 10.3.1.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4057ms
rtt min/avg/max/mdev = 0.407/0.641/0.894/0.180 ms
```

* voici la table arp de john:

```
[baptiste@localhost ~]$ sudo arp
[sudo] password for baptiste:
Address                  HWtype  HWaddress           Flags Mask            Iface
10.3.1.12                ether   08:00:27:e3:40:12   C                     enp0s8
10.3.1.1                 ether   0a:00:27:00:00:0b   C                     enp0s8
_gateway                         (incomplete)                              enp0s8
```
* voici l'adresse MAC de marcel:
```

ip a
    ...
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e3:40:12 brd ff:ff:ff:ff:ff:ff
    ...
```
* l'adresse mac marcel est "08:00:27:e3:40:12" on la retrouve bien dans la table arp de john.



* **La commande "sudo ip -s -s neigh flush all" permet de vider la table arp da la vm**

## Cette commande permet de capturer  les trames du réseau qui passent par:
```
sudo tcpdump -n -w marcel.pcap | ping 10.3.1.11
```
* [**tp3_marcel_arp.pcap**](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp3_reseau_b2/tp3_marcel_arp.pcap)

nous pouvons constater l'échange des requètes arp entre marcel et john.

```
sudo tcpdump -n -w john.pcap | ping 10.3.1.12
```
* [**tp3_john_arp.pcap**](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp3_reseau_b2/tp3_john_arp.pcap)

nous pouvons aussi constater l'échange des requètes arp entre john et marcel.


# **II. Routage**

on configure le gateway, pour diriger les flux vers le router :
```
sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
```
* john :
```
IPADDR=10.3.1.11
NETMASK=255.255.255.0

GATEWAY=10.3.1.254
```

* marcel :
```
IPADDR=10.3.2.12
NETMASK=255.255.255.0

GATEWAY=10.3.2.254
```

(vm router)
enp0s8: => 10.3.1.254/24
enp0s9: => 10.3.2.254/24

```
[baptiste@localhost ~]$ cat /proc/sys/net/ipv4/ip_forward
0
```
l'IP forwarding est désactivé

pour activer ça :

```
sudo nano /etc/sysctl.d/routeur.conf
```

on y inscrit la ligne "net.ipv4.ip_forward=1"

```
sysctl -p /etc/sysctl.d/routeur.conf
```

* l'IP forwarding est maintenant activé :

```
cat /proc/sys/net/ipv4/ip_forward
1
```

* on peut ping marcel depuis john.

```
[baptiste@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
...
--- 10.3.2.12 ping statistics ---
7 packets transmitted, 7 received, 0% packet loss, time 6028ms
rtt min/avg/max/mdev = 0.675/1.022/1.699/0.296 ms
```

* on peut ping john depuis marcel.

```
[baptiste@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
...
--- 10.3.1.11 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 1.170/2.075/2.457/0.465 ms
```

avant le scan il faut vider les tables arp "sudo ip -s -s neigh flush all"

depuis john :
```
ping 10.3.1.11
```

depuis le router :

```
 sudo tcpdump -i any -w tp3_routage_marcel.pcap
```

* [**tp3_routage_marcel.pcap**](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp3_reseau_b2/tp3_routage_marcel.pcap)

il y a eu 8 étapes :
* 1. marsel demande qui est le router 
* 2. retour de l'adresse mac du router
* 3. john demande qui est le router
* 4. retour de l'adresse mac du router
* 5. le router demande qui est john
* 6. retour de l'adresse mac de john
* 7. le router demande qui est marcel
* 8. retour de l'adresse mac de marcel

| ordre | type trame | IP source | MAC source | IP destination | MAC destination |
| -------- | -------- | -------- | -------- | -------- | -------- |
| 1 | ping | 10.3.1.11 | X | 10.3.2.12 | X |
| 2 | ping | 10.3.2.12 | X | 10.3.1.11 | X |
| 3 | arp | 10.3.2.12 | 08,00,27,e3,40,12 | 10.3.2.254 | X |
| 4 | arp | 10.3.2.254 | 08,00,27,90,b6,19 | 10.3.2.12 | 08,00,27,e3,40,12 |
| 5 | arp | 10.3.1.11 | 08,00,27,45,cf,67 | 10.3.1254 | X |
| 6 | arp | 10.3.1.254 | 08,00,27,9b,8c,e6 | 10.3.1.11 | 08,00,27,45,cf,67 |
| 7 | arp | 10.3.1.254 | 08,00,27,9b,8c,e6 | 10.3.1.11 | X |
| 8 | arp | 10.3.2.254 | 08,00,27,90,b6,19 | 10.3.2.12 | X |
| 9 | arp | 10.3.2.12 | 08,00,27,e3,40,12 | 10.3.2.254 | 08,00,27,90,b6,19 |
| 10     | arp     | 10.3.1.11 | 08,00,27,45,cf,67 | 10.3.1.254 | 08,00,27,9b,8c,e6 |



* voici les deux trames d'un ping

* [**tp2_routage_internet.pcap**](https://gitlab.com/baptgdb/tp_reseau_b2/-/blob/main/tp3_reseau_b2/tp2_routage_internet.pcap)

| ordre | type trame | IP source | MAC source | IP destination | MAC destination |
| -------- | -------- | -------- | -------- | -------- | -------- |
| 1 | ping | 10.3.1.11 | X | 10.3.2.12 | X |
| 2 | ping | 10.3.2.12 | X | 10.3.1.11 | X |


## III. DHCP 


toutes les vm sauf router
route-enp0s8
* 10.3.1.11/24 via 10.3.1.254 dev eth0
* 10.3.1.11/24 via 10.3.1.254 dev enp0s3

route-enp0s9
* 10.3.2.22/24 via 10.3.2.254 dev eth0
* 10.3.2.22/24 via 10.3.2.254 dev enp0s3



dans le router :

```
[baptiste@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[baptiste@localhost ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s9 enp0s8 enp0s3

[baptiste@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success

[baptiste@localhost ~]$  sudo firewall-cmd --add-masquerade --zone=public --permanent
success

```

* sudo dnf install dhcp-server -y
* sudo nano /etc/dhcp/dhcpd.conf

```
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;

authoritative;

subnet 10.3.1.1 netmask 255.255.255.0 {
range 10.3.1.12 10.3.1.253;
option routers 10.3.1.254;
option subnet-mask 255.255.255.0;
option domain-name-servers 10.1.1.1, 8.8.8.8;
}
```

* *sudo firewall-cmd --add-port=67/udp --permanent*
* *sudo firewall-cmd --reload*
* *sudo systemctl enable sshd*
* *sudo systemctl restart sshd*

sur bob:
```
sudo dhclient enp0s8
sudo: dhclient: command not found
```

* ### je n'ai pas réussi à aller plus loin dans le Tp je suis bloqué à dhclient.
