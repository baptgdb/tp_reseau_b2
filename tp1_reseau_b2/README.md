# tp 1 réseau.

## Table of Contents

[TOC]

## I. Exploration locale en solo

il y a quelques semaines, j'ai eu un problème avec mon repo git eccepté le Tp 1 tout les autres sont complets.
pout ce rendu de Tp j'ai refait les parties 1 et 3 dont il me manquaient les notes.

```
ipconfig
...
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : home
   Adresse IPv6. . . . . . . . . . . . . .: 
   ...
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.16
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : fe80::d6f8:29ff:fe56:9880%23
                                       192.168.1.1

Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
```
ma carte Ethernet Ethernet est déconnecté... 

la passerelle d'ynov est 10.33.19.254.

un routé permet de connecter plusieurs réseaux. Notre passerelle est une interface réseau du router, qui nous permet de contacter des machines dans d'autres réseaux

on peut accéder a ces informations et les changer dans "Panneau de configuration\Réseau et Internet\Connexions réseau"

## II. Exploration locale en duo
```
mon pc va être en 192.168.0.2, le pc connecté a internet sera en 192.168.0.1 et la gateway sera en 192.168.0.3 avec un masque 255.255.255.252
```
Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
Réponse de 192.168.0.1 : octets=32 temps=344 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128
```
je peut ping l'aure pc, testons avec google :
```
Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=113
Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=113
Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=113
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=113
```

on peut uttiliser un traceroute (tracert 8.8.8.8 (pour dns.google )).

```
Détermination de l’itinéraire vers dns.google [8.8.8.8]
avec un maximum de 30 sauts :

  1    <1 ms    <1 ms    <1 ms  MSI [192.168.0.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     7 ms     7 ms     6 ms  10.33.19.254
  4     5 ms     5 ms     4 ms  137.149.196.77.rev.sfr.net [77.196.149.137]
  5    10 ms     8 ms     9 ms  108.97.30.212.rev.sfr.net [212.30.97.108]
  6    23 ms    22 ms    23 ms  222.172.136.77.rev.sfr.net [77.136.172.222]
  7    24 ms    22 ms    22 ms  221.172.136.77.rev.sfr.net [77.136.172.221]
  8    25 ms    26 ms    25 ms  186.144.6.194.rev.sfr.net [194.6.144.186]
  9    22 ms    27 ms    22 ms  186.144.6.194.rev.sfr.net [194.6.144.186]
 10    24 ms    23 ms    22 ms  72.14.194.30
 11    22 ms    22 ms    22 ms  172.253.69.49
 12    27 ms    22 ms    38 ms  108.170.238.107
 13    23 ms    22 ms    22 ms  dns.google [8.8.8.8]
```

avec netcat on peut lancer un chat 
    - on ecrit "192.168.0.2 8888" dans la console de netcat sur le pc client.
    - on ecrit "-l -p 8888" dans la console de netcat sur le pc server.

```
  TCP    192.168.0.2:9999       192.168.0.1:1055       ESTABLISHED
```

pour pouvoir acceder a ce service via ethernet et wifi, pour pouvoir préciser l'IP d'écoute il faut lancer netcat avec cette commande : 

```
.\nc.exe -l -p 9999 -s 192.168.0.2
```

PS C:\WINDOWS\system32> netstat -a -n -b  | select-string 9999

le ping a déjà une règle nomé **"Analyse de l’ordinateur virtuel (Demande d’écho - Trafic entrant ICMPv4)"** dans le fire-wall qui permet le ping

il suffit après de créer une règle pour associet à netcat le port 9999 au service.
je l'ai appelé **"tcp netcat"**
```

## III. Manipulations d'autres outils/protocoles côté client

voici la route vers le serveur dns 8.8.8.8
```
tracert 8.8.8.8

Détermination de l’itinéraire vers dns.google [8.8.8.8]
avec un maximum de 30 sauts :

  1     5 ms    12 ms     5 ms  livebox.home [192.168.1.1]
  2     6 ms     6 ms     6 ms  80.10.233.85
  3     *        6 ms     5 ms  lag-10.nebrd01z.rbci.orange.net [193.253.94.114]
  4     7 ms     7 ms     8 ms  ae95-0.ncbor201.rbci.orange.net [193.253.93.190]
  5    10 ms    12 ms    10 ms  ae42-0.nipoi201.rbci.orange.net [193.252.100.25]
  6    11 ms     9 ms     9 ms  ae40-0.nipoi202.rbci.orange.net [193.252.160.46]
  7    16 ms    15 ms    15 ms  193.252.137.14
  8    16 ms    16 ms    16 ms  google-44.gw.opentransit.net [193.251.255.104]
  9    19 ms    18 ms    21 ms  108.170.235.161
 10    18 ms    16 ms    16 ms  142.251.64.131
 11    16 ms    16 ms    16 ms  dns.google [8.8.8.8]
```

mon bail DHCP actuel:
```
ipconfig /all
...
   Bail obtenu. . . . . . . . . . . . . . : mercredi 19 octobre 2022 21:04:28
   Bail expirant. . . . . . . . . . . . . : jeudi 20 octobre 2022 21:23:05
...
```

### DNS


La commande "nslookup" retourne les informations de notre serveur dns.
Nslookup suivi d'un nom de domaine retourn l'adresse IP de l'hôte et de son serveur dans.
et réciproquement quand Nslookup est suivi d'une adresse IP.

voici l'adresse ip derrière le nom de domaine "google.com"
```
Nom :    google.com
Addresses:  2a00:1450:4007:809::200e
          142.250.178.142
          
```

voici les adresses ip derrière le nom de domaine "ynov.com"
```
Nom :    ynov.com
Addresses:  2606:4700:20::ac43:4ae2
          2606:4700:20::681a:ae9
          2606:4700:20::681a:be9
          104.26.11.233
          172.67.74.226
          104.26.10.233
```

voici le nom de domaine derrière l'adresses "78.74.21.21"
```
...
Nom :    host-78-74-21-21.homerun.telia.com
```

depuis chez moi je ne parvient pas a trouver le nom de domaine de "92.146.54.88" avec nslookup.

## IV. Wireshark

on peut voir un ping vers la passerelle:

![](https://i.imgur.com/7p9kgUg.png)


# fin
